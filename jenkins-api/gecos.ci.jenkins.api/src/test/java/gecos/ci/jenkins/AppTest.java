package gecos.ci.jenkins;

import java.io.IOException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     * @throws InterruptedException 
     * @throws IOException 
     */
    public void testApp() throws InterruptedException, IOException
    {
    	String url = "https://ci.inria.fr/gecos";
    	String user = "gecos-ci-bot@users.gforge.inria.fr";
    	String passwd = "xxx";
		String mbName = "gecos-htg";
		String commitSha = "c6c11b955f53e8e1fc84e252a76bb900cecd056a";
		String branchName = "master";
		String artifactDownloadPath = "artifacts__updatesite";
		
		
//		JenkinsConnection connection = new JenkinsConnection();
//		connection.connect(url, user, passwd);
//		
//		MultiBranchJob mbJob = connection.findMultibranchJob(mbName);
//		System.out.println(mbJob.getFolderJob().getUrl());
//		
//    	Build lastBuild = connection.triggerJob(mbJob.getFolderJob());
//		
//    	System.out.println(lastBuild.details().getUrl());
		
//		"https://ci.inria.fr/gecos"  "gecos-ci-bot@users.gforge.inria.fr"  "xxx"  "gecos-core" "1113115a7e777d1cdefec454da9375ea5bee0579" "master"  "artifacts__updatesite"
		
		//TODO
//		try {
//			JenkinsApi.main(new String[] {url, user, passwd, mbName, commitSha, branchName, artifactDownloadPath});
//		} catch (Exception e) {
//			Assert.fail();
//		}
        assertTrue( true );
    }
}
