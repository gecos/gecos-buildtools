package gecos.ci.jenkins;

import java.io.IOException;
import java.util.Map;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.FolderJob;
import com.offbytwo.jenkins.model.Job;
import com.offbytwo.jenkins.model.JobWithDetails;
import com.offbytwo.jenkins.model.QueueReference;

public class MultiBranchJob {
	
	private FolderJob folderJob;
	
	public static MultiBranchJob getMultibranchJob(JenkinsServer server, String mbName) throws IOException {
		FolderJob mbJob = server.getFolderJob(server.getJob(mbName)).orNull();
		if(mbJob == null)
			throw new IOException("Failed to find MultiBranch Job '" + mbName + "'");
		return new MultiBranchJob(mbJob);
	}
	
	public MultiBranchJob(FolderJob mbJob) {
		this.folderJob = mbJob;
	}
	
	public FolderJob getFolderJob() {
		return folderJob;
	}
	
	public Map<String, Job> getBranches() {
		return folderJob.getJobs();
	}
	
	public Job getBranch(String branchName) {
		return getBranches().get(branchName);
	}

	public QueueReference build() throws IOException {
		return folderJob.build();
	}

	public JobWithDetails details() throws IOException {
		return folderJob.details();
	}
	
}