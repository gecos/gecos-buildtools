package gecos.ci.jenkins;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.Artifact;
import com.offbytwo.jenkins.model.Build;
import com.offbytwo.jenkins.model.BuildResult;
import com.offbytwo.jenkins.model.BuildWithDetails;
import com.offbytwo.jenkins.model.Job;
import com.offbytwo.jenkins.model.JobWithDetails;
import com.offbytwo.jenkins.model.Queue;
import com.offbytwo.jenkins.model.QueueItem;
import com.offbytwo.jenkins.model.QueueTask;
import com.offbytwo.jenkins.model.TestResult;

import gecos.ci.jenkins.JenkinsApi.FailMode;

public class JenkinsConnection {

	public static final int MAX_JOB_BUILDS_LOOKUP_NUMBER = 100; // maximum number of (latest) builds to consider when looking for a specific build of a job.

	private static final int DEFAULT_NB_RETRIES = 5;
	
	JenkinsServer server;
	
	/**
	 * Connect to the specified Jenkins server with the specified credentials
	 * and make sure it is running.
	 * 
	 * @param url
	 * @param user
	 * @param passwd
	 * @return a running {@link JenkinsServer} instance.
	 */
	public void connect(String url, String user, String passwd) {
		URI jUrl = null;
    	try {
			jUrl = new URI(url);
		} catch (URISyntaxException e) {
			throw new JenkinsConnectionException("Invalid Jenkins server URL: " + url, e);
		}

    	this.server = new JenkinsServer(jUrl, user, passwd);
		if(!server.isRunning()) {
			throw new JenkinsConnectionException("Failed to connect to Jenkins server: " + url
				+ "\n   - Check that url and credentials are valid."
				+ "\n   - Make sure Jenkins server is running. If not, run it then Retry this job.");
		}
	}
	
	public JenkinsServer getServer() {
		return server;
	}
	
	/**
	 * Retry in case {@link JenkinsConnectionException} is caught
	 */
	public static <T,R> R retreive(T input, Function<T, R> getResult, String description, 
			Function<T, String> inputToString, int nbRetries) {
		R result = null;
		int retryCount = nbRetries;
		while(result == null) {
			try {
				result = getResult.apply(input);
			} catch (JenkinsConnectionException e) {
				retryCount--;
				result = null;
				if(retryCount > 0) {
					JenkinsConnection.warn("Failed to retieve " + description + ". Retrying in 2 sec...");
					sleep(2);
				} else {
					throw new JenkinsConnectionException("Failed to retieve " + description + " for: " + inputToString.apply(input), e);
				}
			}
		}
		return result;
	}
	
	/**
	 * Try to get build details and retry {@code nbRetries} times on failure.
	 * 
	 * @param build
	 * @return build details
	 */
	public BuildWithDetails getBuildDetails(Build build, int nbRetries) {
		Function<Build, BuildWithDetails> getResult = b -> { try { 
			return b.details();
		} catch(IOException e) {
			throw new JenkinsConnectionException(e);
		}};
		return retreive(build, getResult, "build details", b -> b.getUrl(), nbRetries);
	}
	
	/**
	 * Try to get Job details and retry {@code nbRetries} times on failure.
	 * 
	 * @param job
	 * @return job details
	 */
	public JobWithDetails getJobDetails(Job job, int nbRetries) {
		Function<Job, JobWithDetails> getResult = j -> { try { 
			return j.details();
		} catch(IOException e) {
			throw new JenkinsConnectionException(e);
		}};
		return retreive(job, getResult, "job details", j -> j.getUrl(), nbRetries);
	}
	
	/**
	 * Try to get build test results and retry {@code nbRetries} times on failure.
	 */
	public TestResult getBuildTestResults(BuildWithDetails buildDetails, int nbRetries) {
		Function<BuildWithDetails, TestResult> getResult = b -> { try { 
			return b.getTestResult();
		} catch(IOException e) {
			throw new JenkinsConnectionException(e);
		}};
		return retreive(buildDetails, getResult, "build test results", b -> b.getUrl(), nbRetries);
	}
	
	/**
	 * Invoke {@link #getBuildDetails(Build, int)} with {@value #DEFAULT_NB_RETRIES} retries.
	 */
	public BuildWithDetails getBuildDetails(Build build) {
		return getBuildDetails(build, DEFAULT_NB_RETRIES);
	}
	
	/**
	 * Invoke {@link #getJobDetails(Job, int)} with {@value #DEFAULT_NB_RETRIES} retries.
	 */
	public JobWithDetails getJobDetails(Job job) {
		return getJobDetails(job, DEFAULT_NB_RETRIES);
	}
	
	public TestResult getBuildTestResults(BuildWithDetails buildDetails) {
		return getBuildTestResults(buildDetails, DEFAULT_NB_RETRIES);
	}
	
	
	/**
	 * @param server
	 * @return
	 */
	public MultiBranchJob findMultibranchJob(String name) {
    	try {
    		return MultiBranchJob.getMultibranchJob(server, name);
    	} catch(Exception e) {
    		throw new JenkinsConnectionException("Could not find MultiBranch Job '" + name + "' !"
    			+ "\n   - Make sure such job exists on Jenkins server.\n"
    			+ "\n   - If not, create it first and Retry.");
    	}
	}
	
	/**
	 * Trigger a build of the specified {@code job} and wait as long as 
	 * the build is in the queue (i.e. didn't start running yet).
	 * 
	 * @param job
	 * @return the last build of the specified {@code job}.
	 */
	public Build triggerJob(Job job) {
		try {
			/* trigger build */
			info("Triggering build of Jenkins job '" + job.getName() + "' ...");
			job.build();
			sleep(2000);
			
			/* wait as long as the build is in the queue */
//			XXX Does not seem to work i.e. always false even when job is in queue !
//	    	while(job.details().isInQueue()) {
//	    		Thread.sleep(2000);
//	    	}
			QueueItem queueItem = getQueueItemOf(job);
			if(queueItem != null) {
				// job is in queue: wait for it..
				info("Job is in Queue. Waiting for it to start...");
				while(queueItem != null) {
					sleep(2000);
					queueItem = getQueueItemOf(job);
				}
			}
			
	    	/* retrieve the just triggered build (this assumes that the job was not triggered by an external event). */ 
	    	return getJobDetails(job).getLastBuild();
		} catch (IOException e) {
			throw new JenkinsConnectionException("Triggering Job build failed.", e);
		}
	}

	/**
	 * @param job
	 * @return {@link QueueItem} corresponding to the specified job, or null if none is found.
	 */
	public QueueItem getQueueItemOf(Job job) {
		for(QueueItem item : getQueue().getItems()) {
			QueueTask task = item.getTask();
			if(task != null && task.getName().equals(job.getName()))
				return item;
		}
		return null;
	}

	/**
	 * @return the jenkins server build queue.
	 */
	public Queue getQueue() {
		try {
			return server.getQueue();
		} catch (IOException e) {
			throw new JenkinsConnectionException("Failed to get Queue.", e);
		}
	}
	
	/**
	 * Wait for the Build {@code buildDetails} to finish.
	 * If {@code printConsole} is true, it tries to progressively print the build output console.
	 * 
	 * @param buildDetails
	 * @param printConsole
	 * @return the updated {@link BuildWithDetails}
	 */
	public BuildWithDetails waitBuild(BuildWithDetails buildDetails, boolean printConsole) {
		try {
			if(buildDetails.isBuilding()) {
				info("Waiting for build '" + buildDetails.getUrl() + "' to finish ..."
						+ " (estimated duration: " + printTime(buildDetails.getEstimatedDuration()) + ")");
				
				int consoleOutputOffset = 0;
				while(buildDetails.isBuilding()) {
		    		sleep(2000);
		    		buildDetails = getBuildDetails(buildDetails);
		    		
					if(printConsole) {
						String consoleOutput = getBuildConsole(buildDetails);
			    		System.out.print(consoleOutput.substring(consoleOutputOffset));
			    		consoleOutputOffset = consoleOutput.length();
					}
		    	}
				
		    	info("Build is finished.");
	    	}
			return buildDetails;			
		} catch (JenkinsConnectionException e) {
			throw new JenkinsConnectionException("Failed while waiting Job build.", e);
		}
	}

	/**
	 * 
	 * @param mbJob
	 * @param commitSha
	 * @param branchName
	 * @return
	 */
	public BuildWithDetails findBranchBuild(MultiBranchJob mbJob, String commitSha, String branchName) {
		try {
			Build build = null;
	    	info("Looking for branch job '" + branchName + "' ...");
	    	Job job = mbJob.getBranch(branchName);
	    	if(job != null) {
	    		info("Found it!");
	    		info("Looking for build with commit sha: " + commitSha + " ...");
				build = findJobLatestBuildByCommitSha(job, commitSha, MAX_JOB_BUILDS_LOOKUP_NUMBER);
	    	} else {
	    		// job name is not correct (e.g. CI_COMMIT_REF_NAME is a TAG name (not a branch))
	    		// => try to lookup in recent builds of all branches
	    		info("Did not find it!");
	    		info("Trying to lookup all branches for build with commit sha: " + commitSha + " ...");
	    		build = findBranchBuildByCommitSha(mbJob, commitSha, 5);
	    	}
	    	
	    	if(build == null) {
	    		throw new JenkinsConnectionException("Could not find a Jenkins build for the specified commit sha !");
	    	}
	    	BuildWithDetails buildDetails = getBuildDetails(build);
	    	info("Found it! Build url: " + buildDetails.getUrl());
	    	return buildDetails;
		} catch (IOException e) {
			throw new JenkinsConnectionException("Failed while trying to find Job build.", e);
		}
	}
	
	public Build findBranchBuildByCommitSha(MultiBranchJob mbJob, String commitSha, int depth) throws IOException {
		depth = depth > 0 ? depth : 1;
		int limit = 1;
		Map<String, Job> branches = mbJob.getBranches();
		while(limit <= depth) {
			for(String branchName : branches.keySet()) {
				Job branch = branches.get(branchName);
				Build build = findJobLatestBuildByCommitSha(branch, commitSha, limit);
				if(build != null)
					return build;
			}
			limit ++;
		}
		return null;
	}

	/**
	 * Check the {@code build} result.
	 * Throws {@link JenkinsConnectionException} if it has failed i.e. 
	 * if build result is neither 'SUCCESS' nor 'UNSTABLE'.
	 * 
	 * @param buildDetails the build
	 */
	public void checkBuildResult(BuildWithDetails buildDetails) {
    	if(!buildSucceeded(buildDetails.getResult())) {
    		throw new JenkinsConnectionException("Build has failed with status: " + buildDetails.getResult()
				+ "\n   - The full Jenkins build output console can be found here: " +  buildDetails.getUrl() + "/console"
				+ "\n   - NOTE: If the failure cause is a side effect (e.g. network connection), try to rerun"
				+ "\n       the jenkins build and if it succeeds retry this gitlab build to fetch artifacts.");
    	}
    	info("Build has succeded with status: " + buildDetails.getResult());
	}
	
	public void checkBuildTestResults(BuildWithDetails buildDetails, List<FailMode> failMode) throws TestFailException {
		TestResult testReport = null;
		try {
			testReport = getBuildTestResults(buildDetails);
		} catch(JenkinsConnectionException e) {
			if(failMode.contains(FailMode.NO_TEST))
				throw new TestFailException("Failed because no tests was run!");
			else throw e;
		}
		
    	int skip = testReport.getSkipCount();
		int pass = testReport.getPassCount();
		int fail = testReport.getFailCount();
		
		int run = pass + fail;
		int total = run + skip;
		
		info("Test results: "
			+ "\n   - Pass: " + pass + " / " + run
			+ "\n   - Fail: " + fail + " / " + run
			+ "\n   - Skip: " + skip + " / " + total
		);
		
		if(failMode.contains(FailMode.NO_TEST))
			if(run == 0) throw new TestFailException("Failed because no tests was run (they might all have been skiped)!");
		if(failMode.contains(FailMode.FAIL))
			if(fail != 0) throw new TestFailException("Failed because some tests have failed!");
		if(failMode.contains(FailMode.SKIP))
			if(skip != 0) throw new TestFailException("Failed because some tests were skipped!");
		
		//TODO: Fail if a regression has occurred and Warn if failing test now passing..
		
	}
	
	/**
	 * Try to download all {@code build} artifacts to the local directory {@code localDownloadDir}.
	 * It skips artifacts that fail to download.
	 * The {@code localDownloadDir} is always created.
	 *  
	 * @param buildDetails the build
	 * @param localDownloadDir local directory path where the download artifacts will be placed.
	 */
	public void downloadBuildArtifacts(BuildWithDetails buildDetails, String localDownloadDir) {
		File dest = new File(localDownloadDir);
    	dest.mkdirs();
    	
    	List<Artifact> artifacts = buildDetails.getArtifacts();
		if(artifacts.isEmpty())
    		warn("Jenkins build has no artifacts!");
    	artifacts.forEach(a -> downloadArtifact(buildDetails, a, dest));    	
	}
	
	private void downloadArtifact(BuildWithDetails build, Artifact a, File toDir) {
		info("Downloading artifact: " + a.getRelativePath() + " ...");
	
		Function<BuildWithDetails, Boolean> getResult = b -> { try { 
			InputStream in = b.downloadArtifact(a);
			Files.copy(in, new File(toDir, a.getFileName()).toPath(), StandardCopyOption.REPLACE_EXISTING);
			return true;
		} catch (FileSystemException e) {
			throw new RuntimeException("Couldn't acces local file", e);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		} catch(IOException e) {
			throw new JenkinsConnectionException(e);
		}};

//		try {
			retreive(build, getResult, "artifact '" + a.getRelativePath() + "'", b -> b.getUrl(), DEFAULT_NB_RETRIES);
//		} catch (Exception e) {
//			if(fail)
//				throw e;
//			warn("Failed to download artifact: '" + a.getRelativePath() + "'. Skipping!\n" + ExceptionUtils.getStackTrace(e));
//		}
	}
	
	
//--------------------------------------------------------------------
	
	public Build findJobLatestBuildByCommitSha(Job job, String commitSha, int limit) {
		List<Build> builds = getJobDetails(job).getBuilds();
		if(builds.isEmpty()) {
			info("Job has no builds! Triggering job ...");
			triggerJob(job);
			builds = getJobDetails(job).getBuilds(); //! update buildDetails
		}
		
		//XXX assume that getBuilds() return the latest build first
		return builds.stream()
			.limit(limit)
			.filter(b -> getCommitSHA(b).startsWith(commitSha))
			.findFirst().orElse(null);
	}

	public boolean buildSucceeded(BuildResult result) {
		return result.equals(BuildResult.SUCCESS) 
			|| result.equals(BuildResult.UNSTABLE);
	}

	public String getBuildConsole(BuildWithDetails buildDetails) {
		try {
			return buildDetails.getConsoleOutputText();
		} catch (IOException e) {
			// if failed to read console: continue anyway.
		}
		return "";
	}
	

	/**
	 * Lookup lastBuildRevision commit SHA at [BUILD]/action/lastBuiltRevision/SHA1.
	 * 
	 * @param b build
	 * @return commit SHA
	 */
	@SuppressWarnings("unchecked")
	public String getCommitSHA(Build b) {
		final String key = "lastBuiltRevision";
		
		try {
			BuildWithDetails build = getBuildDetails(b);
			List<Map<String, Object>> actions = build.getActions();
			Map<String, Object> buildDataAction = actions.stream()
				.filter(a -> a.containsKey(key))
				.findFirst().orElse(null);
			
			Map<String, Object> obj = (Map<String, Object>) buildDataAction.get(key);
			String sha = (String) obj.get("SHA1");
			return sha;
		} catch (Exception e) {
			throw new JenkinsConnectionException("Failed to find Build commit SHA!" + e);
		}
	}
	
//--------------------------------------------------------------------
	private static void sleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static String printTime(long millis) {
		long totalSecs = millis / 1000;
		long hours = totalSecs / 3600;
		long minutes = (totalSecs % 3600) / 60;
		long seconds = totalSecs % 60;
		return String.format("%02d:%02d:%02d", hours, minutes, seconds);
	}
	
	public static void info(String msg) {
		msg = "[JenkinsApi][INFO] " + msg;
		System.out.println(msg);
	}
	
	public static void warn(String msg) {
		msg = "[JenkinsApi][WARNING] " + msg;
		System.out.println(msg);
	}
	
	public static void error(String msg) {
		msg = "[JenkinsApi][ERROR] " + msg;
		System.err.println(msg);
	}

}
