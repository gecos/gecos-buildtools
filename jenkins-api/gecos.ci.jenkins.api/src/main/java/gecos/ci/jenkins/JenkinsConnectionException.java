package gecos.ci.jenkins;

public class JenkinsConnectionException extends RuntimeException {

	private static final long serialVersionUID = -596224463793913066L;

	
	public JenkinsConnectionException(String msg) {
		super(msg);
	}
	
	public JenkinsConnectionException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public JenkinsConnectionException(Throwable cause) {
		super(cause);
	}
}
